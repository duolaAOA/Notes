You are given a table, BST, containing two columns: N and P,
where N represents the value of a node in Binary Tree, and P is the parent of N.

                Column        TYPE
                N              Integer
                P              Integer

Write a query to find the node type of Binary Tree ordered by the value of the node.
Output one of the following for each node:
  Root: If node is root node.
  Leaf: If node is leaf node.
  Inner: If node is neither root nor leaf node.

Sample Input

              N         P
              1         2
              3         2
              6         8
              9         8
              2         5
              8         5
              5         null

Sample Output

    1 Leaf
    2 Inner
    3 Leaf
    5 Root
    6 Leaf
    8 Inner
    9 Leaf


Explanation
The Binary Tree below illustrates the sample:
'''
                     5
                   /   \
                  2     8
                 / \   / \
                1   3 6   9
'''

Mysql:
/*  1.这题可以看出P 为Null 就是根节点， 2. 当在P 和 N同时存在时， 就是一个中间节点， 也就是 Inner
    3. 那么另外一种情况就是叶子节点了
    首先使用 CASE WHEN 语句完成
*/
select N, case when P is NULL then 'Root' when N in (select P from BST)
then 'Inner' else 'Leaf' end from BST order by N;

/*
 IF 条件表达式
 */
select N, if(P is null,'Root',if(N in (select P from BST),'Inner','Leaf'))
from BST as B order by N;