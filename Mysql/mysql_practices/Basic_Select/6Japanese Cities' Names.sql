查询CITY表中所有日本城市的名称   日本的COUNTRYCODE是JPN

输入格式

所述CITY表被描述如下：

                      CITY
              Field           Type
              ID              NUMBER
              NAME           VARCHAR2(17)
              COUNTRYCODE     VARCHAR2(3)
              DISTRICT        VARCHAR2(20)
              POPULATION      NUMBER

select NAME from CITY where COUNTRYCODE = 'JPN';