Query a list of

Input Format

The STATION table is described as follows:

                 STATION
            Field       Type
            ID          NUMBER
            CITY        VARCHAR2(21)
            STATE       VARCHAR2(2)
            LAT_N       NUMBER
            LONG W      NUMBER
(where LAT_N is the northern latitude and LONG_W is the western longitude.)


select distinct city from station
where substring(city,1,1) not in ('a','e','i','o','u')
or substring(city,-1,1) not in ('a','e','i','o','u');












