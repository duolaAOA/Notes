按ID查询CITY中城市的所有列   ID=1661

输入格式

所述CITY表被描述如下：

                      CITY
              Field           Type
              ID              NUMBER
              NAME           VARCHAR2(17)
              COUNTRYCODE     VARCHAR2(3)
              DISTRICT        VARCHAR2(20)
              POPULATION      NUMBER

select * from CITY where id=1661;