Query the two cities in STATION with the shortest and longest CITY names,
as well as their respective lengths (i.e.: number of characters in the name).
If there is more than one smallest or largest city, choose the one that comes first when ordered alphabetically.

Input Format

The STATION table is described as follows:

                 STATION
            Field       Type
            ID          NUMBER
            CITY        VARCHAR2(21)
            STATE       VARCHAR2(2)
            LAT_N       NUMBER
            LONG W      NUMBER
(where LAT_N is the northern latitude and LONG_W is the western longitude.)


select CITY, length(CITY) from station order by length(CITY) asc, city limit 1;
select CITY, length(CITY) from station order by length(CITY) desc, city limit 1;











