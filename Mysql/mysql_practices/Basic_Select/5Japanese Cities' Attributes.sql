在CITY表中查询每个日本城市的所有属性   日本的COUNTRYCODE是JPN

输入格式

所述CITY表被描述如下：

                      CITY
              Field           Type
              ID              NUMBER
              NAME           VARCHAR2(17)
              COUNTRYCODE     VARCHAR2(3)
              DISTRICT        VARCHAR2(20)
              POPULATION      NUMBER

select * from CITY where COUNTRYCODE = 'JPN';